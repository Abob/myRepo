package com.itheima.service;

import com.itheima.domain.Patient;

import java.util.List;

public interface IPatientService {
    List<Patient> findAll();
    List<Patient> findOne(String name);
}
