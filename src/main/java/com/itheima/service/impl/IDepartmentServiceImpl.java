package com.itheima.service.impl;

import com.itheima.domain.Department;
import com.itheima.domain.Patient;
import com.itheima.mapper.DepartmentMapper;
import com.itheima.service.IDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @PackageName: com.itheima.service.impl
 * @ClassName: IPatientServiceImpl
 * @Author: Bob
 * @Date: 2020-01-05 9:35
 * @Description: //TODO
 */
@Service
public class IDepartmentServiceImpl implements IDepartment {

    @Autowired
    private DepartmentMapper mapper;

    @Override
    public List<Department> findAll() {
        return mapper.findAll();
    }

    @Override
    public void save(Patient patient) {
        mapper.save(patient);
    }
}
