package com.itheima.service.impl;

import com.itheima.domain.Patient;
import com.itheima.mapper.PatientMapper;
import com.itheima.service.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @PackageName: com.itheima.service.impl
 * @ClassName: IPatientImpl
 * @Author: Bob
 * @Date: 2020-01-05 11:12
 * @Description: //TODO
 */
@Service
public class IPatientServiceImpl implements IPatientService {

    @Autowired
    private PatientMapper mapper;

    @Override
    public List<Patient> findAll() {
        return mapper.findAll();
    }

    @Override
    public List<Patient> findOne(String name) {
        return mapper.findOne(name);
    }
}
