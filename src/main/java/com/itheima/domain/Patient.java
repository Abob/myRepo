package com.itheima.domain;

import java.io.Serializable;

/**
 * @PackageName: com.itheima.domain
 * @ClassName: Patient
 * @Author: Bob
 * @Date: 2020-01-05 9:20
 * @Description: //TODO
 */
public class Patient implements Serializable {
    private Long p_id;
    private String p_name;
    private String p_sex;
    private Long p_dep;
    private String p_desc;
    private String p_deptname;
    private Department department;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Long getP_id() {
        return p_id;
    }

    public void setP_id(Long p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_sex() {
        return p_sex;
    }

    public void setP_sex(String p_sex) {
        this.p_sex = p_sex;
    }

    public Long getP_dep() {
        return p_dep;
    }

    public void setP_dep(Long p_dep) {
        this.p_dep = p_dep;
    }

    public String getP_desc() {
        return p_desc;
    }

    public void setP_desc(String p_desc) {
        this.p_desc = p_desc;
    }

    public String getP_deptname() {
        return p_deptname;
    }

    public void setP_deptname(String p_deptname) {
        this.p_deptname = p_deptname;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "p_id=" + p_id +
                ", p_name='" + p_name + '\'' +
                ", p_sex='" + p_sex + '\'' +
                ", p_dep=" + p_dep +
                ", p_desc='" + p_desc + '\'' +
                ", p_deptname='" + p_deptname + '\'' +
                ", department=" + department +
                '}';
    }
}
