package com.itheima.domain;

import java.io.Serializable;

/**
 * @PackageName: com.itheima.domain
 * @ClassName: Department
 * @Author: Bob
 * @Date: 2020-01-05 9:22
 * @Description: //TODO
 */
public class Department implements Serializable {

    private Long dep_id;
    private String dep_name;

    public Long getDep_id() {
        return dep_id;
    }

    public void setDep_id(Long dep_id) {
        this.dep_id = dep_id;
    }

    public String getDep_name() {
        return dep_name;
    }

    public void setDep_name(String dep_name) {
        this.dep_name = dep_name;
    }

    @Override
    public String toString() {
        return "Department{" +
                "dep_id=" + dep_id +
                ", dep_name='" + dep_name + '\'' +
                '}';
    }
}
