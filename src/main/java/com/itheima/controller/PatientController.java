package com.itheima.controller;

import com.itheima.domain.Patient;
import com.itheima.service.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @PackageName: com.itheima.controller
 * @ClassName: PatientController
 * @Author: Bob
 * @Date: 2020-01-05 11:08
 * @Description: //TODO
 */
@Controller
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private IPatientService service;

    @RequestMapping("/findAll")
    public ModelAndView findAll(ModelAndView modelAndView) {
        List<Patient> patientList = service.findAll();
        modelAndView.addObject("patientList",patientList);
        modelAndView.setViewName("patients-list");
        return modelAndView;
    }

    @RequestMapping("/findOne")
    public ModelAndView finOne(String name) {
        StringBuilder names = new StringBuilder();
        String p_name = names.append("%").append(name).append("%").toString();
        ModelAndView modelAndView = new ModelAndView();
        List<Patient> patientList = service.findOne(p_name);
        modelAndView.addObject("patientList",patientList);
        modelAndView.setViewName("patients-list");
        return modelAndView;
    }
}
