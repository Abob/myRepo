package com.itheima.controller;

import com.itheima.domain.Department;
import com.itheima.domain.Patient;
import com.itheima.service.IDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @PackageName: com.itheima.controller
 * @ClassName: PatientController
 * @Author: Bob
 * @Date: 2020-01-05 9:40
 * @Description: //TODO
 */
@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartment service;

    @RequestMapping("/findAll")
    public ModelAndView findAll() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("subscribe-form");
        List<Department> departmentList = service.findAll();
        modelAndView.addObject("departmentList",departmentList);
        return modelAndView;
    }

    @RequestMapping("/save")
    public String save(Patient patient) {
        service.save(patient);
        return  "redirect:/department/findAll";
    }

}
