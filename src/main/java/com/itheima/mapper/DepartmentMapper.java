package com.itheima.mapper;

import com.itheima.domain.Department;
import com.itheima.domain.Patient;

import java.util.List;

/**
 * @Description //TODO
 * @Date 2020-01-06 12:51
 * @Param
 * @return
 * @Author: Bob
 **/
public interface DepartmentMapper {
    /**
     * 查找所有科室信息
     * @return
     */

    List<Department> findAll();

    /**
     * 挂号保存患者信息
     * @param patient
     */
    void save(Patient patient);
}
