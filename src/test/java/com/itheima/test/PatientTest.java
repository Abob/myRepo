package com.itheima.test;

import com.itheima.domain.Patient;
import com.itheima.mapper.PatientMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @PackageName: com.itheima.test
 * @ClassName: PatientTest
 * @Author:Administrator
 * @Date: 2020-01-07 8:58
 * @Description: //TODO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class PatientTest {

    @Autowired
    private PatientMapper mapper;

    @Test
    public void test() {
        List<Patient> patientList = mapper.findOne("%李%");
        for (Patient patient : patientList) {
            System.out.println(patient);
        }
    }

}
